# VizworX Renovate Configuration _(@vizworx/renovate-config)_

[![License][license-image]][license-url]

> A renovate config with common settings for VizworX

This is a collection of [Renovate config presets](https://docs.renovatebot.com/config-presets/) that are used on [VizworX](https://vizworx.com) projects. You can extend the base config, or any of the named configs.

## Usage

You don't need to install this package, since Renovate will automatically pull it in if you extend `@vizworx` or `@vizworx:subPackageName`

**Recommended configuration for projects**:

This will enable all of the configurations listed below, however you can use them individually if you would prefer.

`renovate.json`

```json
{
  "extends": [
    "gitlab>vizworx/public/renovate-config"
  ]
}
```

### Renovate Configuration

`renovate.json`

**Labels**:

Automatically apply common VizworX labels such as `🤖 Dependency` to PRs

```json
{
  "extends": [
    "gitlab>vizworx/public/renovate-config:labels"
  ]
}
```

**Node**:

Always update to the latest LTS of Node

```json
{
  "extends": [
    "gitlab>vizworx/public/renovate-config:node"
  ]
}
```

**Automerge**:

Pinned PRs should be automerged, and should skip the CI system

```json
{
  "extends": [
    "gitlab>vizworx/public/renovate-config:automerge"
  ]
}
```

**Stability**:

Wait a few days to help ensure new dependency versions are stable, and that new patches aren't being released.

* Major - Wait 7 days
* Minor - Wait 2 days
* Patch - Wait 2 days

```json
{
  "extends": [
    "gitlab>vizworx/public/renovate-config:stability"
  ]
}
```

**Show All**:

Remove limits on the number of dependency PRs that can exist at one time, or be created in one hour, and enable the Master Issue to keep track of upcoming (unstable) PRs

```json
{
  "extends": [
    "gitlab>vizworx/public/renovate-config:showAll"
  ]
}
```

**Docker**:

Adjust default behaviours for RenovateBot and Docker, primarily disabling creation of major updates for docker which became default in `v25.0.0`.

```json
{
  "extends": [
    "gitlab>vizworx/public/renovate-config:docker"
  ]
}
```

**Group - React**:

Group all React-related packages together to ensure we don't have a mismatch between closely linked dependencies such as `react` and `react-dom`, or `react-dom` and `@hot-loader/react-dom`.

```json
{
  "extends": [
    "gitlab>vizworx/public/renovate-config:groupReact"
  ]
}
```

**Group - All**:

Enable commonly used package groups

```json
{
  "extends": [
    "gitlab>vizworx/public/renovate-config:groupAll"
  ]
}
```

**Outside Business Hours**:

Only run Renovate outside of business hours, to reduce the amount of PR noise and CI load generated during the day. Renovate will run before 5am and after 10pm on weekdays, and any time during the weekends.

```json
{
  "extends": [
    "gitlab>vizworx/public/renovate-config:outsideBusinessHours"
  ]
}
```

**Quiet Noisy Packages**:

Some packages, such as the [AWS SDK](https://www.npmjs.com/package/aws-sdk) release a new package on a frequent schedule. This will limit them to only update once a week (on Monday before 3am).

```json
{
  "extends": [
    "gitlab>vizworx/public/renovate-config:quietNoisyPackages"
  ]
}
```
